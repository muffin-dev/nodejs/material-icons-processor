import { IIconStyle } from '.';
import { join } from 'path';

/**
 * @const INPUT_DIRECTORY The Material Icon's /png directory.
 */
export const INPUT_DIRECTORY = join(__dirname, '../material-icons/png');

export const ICON_STYLES: Record<string, IIconStyle> = {
  filled: { dir: 'materialicons', prefix: 'baseline' },
  outlined: { dir: 'materialiconsoutlined', prefix: 'outline' },
  rounded: { dir: 'materialiconsround', prefix: 'round' },
  sharp: { dir: 'materialiconssharp', prefix: 'sharp' },
  twoTone: { dir: 'materialiconstwotone', prefix: 'twotone' }
}

export enum EIconSize {
  xSmall = '18dp',
  small = '24dp',
  medium = '36dp',
  large = '48dp',
}

/**
 * @const FORMATS Predefined output file name formats.
 * Symbols:
 *    - %category: The category name, in pascal case
 *    - %name: The icon name, in pascal case
 *    - [...]: The content in the square brackets is added only for the white version of the icon
 *    - {...}: The content in the curly brackets is added only for the 2x version of the icon
 */
export const FORMATS: Record<string, string> = {
  simple: '%category_%name[-white]{@2x}.png',
  unity: 'S_MaterialIcons_%category_%name[-White]{@2x}.png'
}
import { IIconStyle } from '.';
import { EIconSize, ICON_STYLES } from './consts';

/**
 * @interface IProcessorSettings Defines the settings to use for processing the icons of an input directory.
 */
export interface IProcessorSettings {
  /**
   * The output directory, where processed icons will be exported.
   */
  outputDirectory?: string;

  /**
   * The expected icon style.
   */
  targetStyle?: IIconStyle;

  /**
   * The expected icon size.
   */
  targetSize?: EIconSize;

  /**
   * The icon factors to include in the output.
   */
  factors?: ('1x' | '2x')[];

  /**
   * The expected color variations of the output icon.
   */
  colors?: ('black' | 'white')[];

  /**
   * The output file naming format.
   */
  format?: string;

  /**
   * The name of the icons to process. If null or empty, all icons will be processed.
   */
  icons?: string[];
}

/**
 * @const DEFAULT_SETTINGS The default processor settings.
 */
export const DEFAULT_SETTINGS: IProcessorSettings = {
  outputDirectory: '',
  targetStyle: ICON_STYLES.outlined,
  targetSize: EIconSize.small,
  factors: [ '1x', '2x' ],
  colors: [ 'black', 'white' ],
  format: '%category_%name[-white]{@2x}',
  icons: null
};
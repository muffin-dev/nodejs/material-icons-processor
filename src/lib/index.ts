import { overrideObject } from '@muffin-dev/js-helpers';
import NodeHelpers, { asAbsolute } from '@muffin-dev/node-helpers';
import { join } from 'path';
import sharp from 'sharp';
import { IProcessorSettings, DEFAULT_SETTINGS, INPUT_DIRECTORY } from '.';

/**
 * Process the input Material Icons directory to extract the icon files, processed and formatted as expected.
 * @param settings The settings to use for processing the icons.
 */
export async function processIcons(settings?: IProcessorSettings) {
  if (settings) {
    const tmpSettings = Object.assign({ }, DEFAULT_SETTINGS);
    overrideObject(tmpSettings, settings);
    settings = tmpSettings;
  }
  else {
    settings = Object.assign({ }, DEFAULT_SETTINGS);
  }

  settings.outputDirectory = asAbsolute(settings.outputDirectory);

  // Find icon categories directories
  const categories = await NodeHelpers.readdirAsync(INPUT_DIRECTORY, true, false);
  for (const category of categories) {
    const icons = await NodeHelpers.readdirAsync(join(INPUT_DIRECTORY, category.name), true, false);
    for (const icon of icons) {
      // Skip if only some icons should be included but the current one doesn't match
      if (settings.icons && settings.icons.length > 0 && !settings.icons.includes(icon.name)) {
        continue;
      }

      const baseIconPath = join(INPUT_DIRECTORY, category.name, icon.name, settings.targetStyle.dir, settings.targetSize);
      const iconFileName = `${settings.targetStyle.prefix}_${icon.name}_black_${settings.targetSize}.png`;

      // Include the 1x version of the icon if required
      if (settings.factors.includes('1x')) {
        await processSingleIcon(join(baseIconPath, '1x', iconFileName), nicifyName(category.name), nicifyName(icon.name), settings, false);
      }
      // Include the 2x version of the icon if required
      if (settings.factors.includes('2x')) {
        await processSingleIcon(join(baseIconPath, '2x', iconFileName), nicifyName(category.name), nicifyName(icon.name), settings, true);
      }
    }
  }
}

/**
 * Processes the given icon, and generate the output file.
 * @param path The absolute path to the icon you want to process.
 * @param category The category name of the icon to process.
 * @param name The name of the icon to process.
 * @param settings The settings to use for processing icons.
 * @param is2x Is the current icon the 2x version?
 */
async function processSingleIcon(path: string, category: string, name: string, settings: IProcessorSettings, is2x: boolean) {
  const iconSharp = sharp(path);
  // Read metadata to check if file exists and is readable
  try {
    await iconSharp.metadata();
  }
  catch (error) {
    console.error(error, path);
    return;
  }

  // Export the original black icon
  if (settings.colors.includes('black')) {
    await iconSharp.toFile(join(settings.outputDirectory, formatName(settings.format, category, name, false, is2x)));
  }

  // Negate and export the white icon if required
  if (settings.colors.includes('white')) {
    await iconSharp
      .negate({ alpha: false })
      .toFile(join(settings.outputDirectory, formatName(settings.format, category, name, true, is2x)));
  }
}

/**
 * Process the given format to make an output icon file name.
 * @param format The expected output file name.
 * @param category The nicified category name.
 * @param name The nicified icon name.
 * @param isWhite Is the current icon the white version?
 * @param is2x Is the current icon the 2x version?
 * @returns Returns the processed file name.
 */
function formatName(format: string, category: string, name: string, isWhite: boolean, is2x: boolean): string {
  while (format.match('%category')) {
    format = format.replace('%category', category);
  }
  while (format.match('%name')) {
    format = format.replace('%name', name);
  }
  while (format.match(/\[.+\]/)) {
    format = format.replace(/\[(.+)\]/, isWhite ? '$1' : '');
  }
  while (format.match(/\{.+\}/)) {
    format = format.replace(/\{(.+)\}/, is2x ? '$1' : '');
  }
  return format;
}

/**
 * Processes the given name to make it more readable or apply a naming convention.
 * @param name The name you want to process.
 * @returns Returns the processed name.
 */
function nicifyName(name: string): string {
  const toUpper = (m: string, offset: string) => m ? offset.toUpperCase() : '';

  // Set first letter as uppercase
  name = name.replace(/^(\w)/, toUpper);
  // Replace any word preceeded by an underscore with an uppercase letter
  while (name.match(/_(\w)/)) {
    name = name.replace(/_(\w)/, toUpper)
  }
  return name;
}



// Exports
export * from './consts';
export * from './interfaces';
export * from './processor-settings';

export default processIcons;
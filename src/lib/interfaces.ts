/**
 * @interface IIconStyle Defines a Material Icon's style directory and name prefix.
 */
export interface IIconStyle {
  /**
   * The directory in which the icon file can be found with a specific style.
   */
  dir: string;

  /**
   * The name prefix of the icon file.
   */
  prefix: string;
}
#!/usr/bin/env node
import { asAbsolute } from '@muffin-dev/node-helpers';
import { Command } from 'commander';
import { EIconSize, FORMATS, ICON_STYLES, IProcessorSettings, processIcons } from '..';
import inquirer from 'inquirer';

const program = new Command();
program
  .name('@muffin-dev/material-icons-processor')
  .usage('[outputPath]')
  .addHelpText('beforeAll', 'Display a CLI utility to help you configure the icons processor.')
  .argument('[outputPath]', 'The directory to which you want the processed icons to be exported.')
  .action(async (outputPath: string) => {
    const answers: IProcessorSettings = { };
    answers.outputDirectory = outputPath ? asAbsolute(outputPath) : null;

    if (!answers.outputDirectory) {
      Object.assign(answers, await inquirer.prompt([
        {
          name: 'outputDirectory',
          type: 'input',
          message: 'Select the output directory, where processed icons will be exported',
          filter: (input: string) => asAbsolute(input)
        }
      ]));
    }
    
    Object.assign(answers, await inquirer.prompt([
      {
        name: 'targetStyle',
        type: 'list',
        message: 'What icon style would you like to process?',
        choices: Object.keys(ICON_STYLES),
        default: 'outlined',
        filter: (input: string) => {
          return ICON_STYLES[input] || ICON_STYLES.outlined;
        }
      },
      {
        name: 'targetSize',
        type: 'list',
        message: 'What icon size would you like to process?',
        choices: [ 'xSmall (18dp)', 'small (24dp)', 'medium (36dp)', 'large (48dp)' ],
        default: 'small (24dp)',
        filter: (input: string) => {
          if (input.startsWith('xSmall')) { return EIconSize.xSmall; }
          if (input.startsWith('medium')) { return EIconSize.medium; }
          if (input.startsWith('large')) { return EIconSize.large; }
          else { return EIconSize.small; }
        }
      },
      {
        name: 'factors',
        type: 'checkbox',
        message: 'What icon size factors would you like to process?',
        choices: [
          {
            key: '1',
            value: '1x',
            checked: true
          },
          {
            key: '2',
            value: '2x',
            checked: true
          }
        ],
      },
      {
        name: 'colors',
        type: 'checkbox',
        message: 'What icon colors would you like to process?',
        choices: [
          {
            key: 'b',
            value: 'black',
            name: 'Black',
            checked: true
          },
          {
            key: 'w',
            value: 'white',
            name: 'White',
            checked: true
          },
        ],
      },
      {
        name: 'icons',
        type: 'input',
        message: 'List the name of the icons you want to process (separated by commas). If let empty, all the icons will be processed.',
        default: '',
        filter: (input: string) => {
          return input.split(/\s*[,;]\s*/);
        }
      },
      {
        name: 'format',
        type: 'list',
        message: 'What is the output file naming format you want to use?',
        choices: [
          {
            key: 's',
            value: FORMATS.simple,
            name: 'Simple: ' + FORMATS.simple
          },
          {
            key: 'u',
            value: FORMATS.unity,
            name: 'Unity: ' + FORMATS.unity
          },
          {
            key: 'c',
            value: 'custom',
            name: 'Custom format...'
          }
        ],
        default: 0,
        filter: (input: string) => {
          if (input === 'custom') {
            input = null;
          }
          return input;
        }
      }
    ]));

    if (!answers.format) {
      Object.assign(answers, await inquirer.prompt([
        {
          name: 'format',
          type: 'input',
          message: 'Write the expected output file naming format for the processed icons. It must end with ".png".'
        }
      ]));
    }

    console.log(answers);
    Object.assign(answers, await inquirer.prompt([
      {
        name: 'proceed',
        type: 'confirm',
        message: 'Proceed?',
        default: true
      }
    ]));

    if (!(answers as any).proceed) {
      return;
    }

    processIcons(answers);
  });

program.parse(process.argv);
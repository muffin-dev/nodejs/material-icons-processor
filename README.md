# Muffin Dev - Material Icons Processor

The Material Icons Processor is a small tool for processing and exporting Material Icons as PNG, with specific rules and naming.

By now, the tool is meant to process a given style and size of icon, and export 1x and 2x black and white versions.

The original icons can be found on [Google's Material Icons GitHub repository](https://github.com/google/material-design-icons/tree/master/png). These icons are already provided in this tool.

## How to use?

You can use this package using `npx`:

```bash
npx @muffin-dev/material-icons-processor
```

This command will run the `bin` program of the package, which will ask you what to do.

## Settings

As the program runs, here are the list of the question you'll have to answer to generate the icons.

- `Output Directory`: Absolute or relative path to the directory that will contain the processed icons.
- `Icon Style`: Possible answers are `filled`, `outlined`, `rounded`, `sharp`, `twotone`, which match with the icon styles you can try on https://fonts.google.com/icons.
- `Icon Size`: Possible answers are `18dp`, `24dp`, `36dp` and `48dp`.
- `Factor`: Select the size factor you want to process, `1x` and/or `2x`.
- `Icon Color`: Select the icon colors you want to process. For now, only `black` and `white` available.
- `Filter`: Names of the icons you want to process. If not defined, all the icons will be processed.
- `Naming Format`: Select a predefined output file name or tell the program you want to create a custom one.
- `Custom Format`: If you selected a custom format, just write the expected output file name.

### Predefined file naming & custom name symbols

For now, the only predefined file name is `unity`, which is `S_MaterialIcons_%category_%name[-White]{@2x}`.

When defining a custom file name format, you can use some symbols that will be replaced with the current processed icons informations:

- `%category`: The category name (the parent directory) of the icon, using PascalCase
- `%name`: The name of the icon itself, using PascalCase
- `[...]`: The content between square brackets is included only when processing the white version of the icon
- `{...}`: The content between curly brackets is included only when processing the 2x version of the icon

As an example, the `unity` naming will produce these results for the `new_label` icon:

- `S_MaterialIcons_Action_NewLabel`
- `S_MaterialIcons_Action_NewLabel@2x`
- `S_MaterialIcons_Action_NewLabel-White`
- `S_MaterialIcons_Action_NewLabel-White@2x`